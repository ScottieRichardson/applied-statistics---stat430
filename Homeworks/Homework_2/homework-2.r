####################################################################################################

# 1) a)
d = c(9.50, 9.80, 8.30, 8.60, 7.00, 17.40, 15.20, 16.70, 15.00, 14.80, 25.60, 24.40, 19.50, 22.80,
      19.80, 8.40, 11.00, 9.90, 6.40, 8.20, 15.00, 16.40, 15.40, 14.50, 13.60, 23.40, 23.30, 21.20,
      21.70, 21.30)

s = c(14814, 14007, 7573, 9714, 5304, 43243, 28028, 49499, 26222, 26751, 96305, 72594, 32207, 70453,
      38138, 17502, 19443, 14191, 8076, 10728, 25319, 41792, 25312, 22148, 18036, 104170, 49512,
      48218, 47661, 53045)

png("1_a.png")
plot(d, s, xlab="Density", ylab="Stiffness", main="Particle Board \n Stiffness vs. Density")
dev.off()

# 1) b)
model = lm(s~d)
model
# The equation for the linear regression model is:
#       s = -25434 + (3885 * d)
png("1_b.png")
plot(d, s, xlab="Density", ylab="Stiffness", main="Particle Board \n Stiffness vs. Density")
abline(model)
dev.off()

# 1) c)
png("1_c.png")
par(mfrow=c(2,2))
plot(model)
dev.off()
# When looking at the graph of the residuals vs fitted it appears that our model is most likely not
#   the optimal model and should be analyzed further.

# 1) d)
library(MASS)
b = boxcox(model)
lambda = round(b$x[which(b$y == max(b$y))], digits=2)
lambda
# Our approximate lmabda value is 0.14
s2 = (s^lambda - 1) / lambda
model2 = lm(s2~d)
model2
# The new equation for the linear regression model is:
#       s2 = 14.7297 + (0.5197 * d)
png("1_d_1.png")
plot(d, s2, main="Transformed Particle Board Linear Model", xlab="Density", ylab="Transformed Stiffness Values")
abline(model2)
dev.off()

png("1_d_2.png")
par(mfrow=c(2,2))
plot(model2)
dev.off()
# When comparing the residuals vs fitted plot from our origional model and the new transformed
#   model, it does indeed appear that we have improved the linear model.

####################################################################################################

# 2)
temp = c(150, 150, 150, 200, 200, 200, 250, 250, 250, 300, 300, 300)
yield = c(77.4, 76.7, 78.2, 84.1, 84.5, 83.7, 88.9, 89.2, 89.7, 94.8, 94.7, 95.9)

model = lm(yield~temp)
model
# The equation for the linear regression model is:
#       yield = 60.2633 + (0.1165 * temp)
png("2_1.png")
plot(temp, yield, main="Chemical Reaction \n Percentage Yield vs. Temperature", xlab="Temperature", ylab="Percentage Yield")
abline(model)
dev.off()

png("2_2.png")
par(mfrow=c(2,2))
plot(model)
dev.off()
# Based on the residuals vs fitted plot is seems that the given linear model should not need
#   further improvement.

####################################################################################################

# 3) a)
age = c(38, 42, 46, 32, 55, 52, 61, 61, 26, 38, 66, 30, 51, 27, 52, 49, 39)
hdl = c(57, 54, 34, 56, 35, 40, 42, 38, 47, 44, 62, 53, 36, 45, 38, 55, 28)
design_matrix = matrix(c(rep(1, length(age)), age), ncol = 2)
design_matrix

# 3) b)
transpose = t(design_matrix)
transpose

# 3) c)
b = solve(transpose %*% design_matrix) %*% transpose %*% hdl
b
# The approximate values for b0 and b1 are:
#   b0 = 50.7841278
#   b1 = -0.1298434

# MSE is an unbiased estimator of sigma squared.
# MSE = SSE / n - 2
# SSE = y^T * (I - H) * y
# H = x * (x^T * x)^-1 * x^T

I = diag(length(age))
H = design_matrix %*% solve(transpose %*% design_matrix) %*% transpose
SSE = t(hdl) %*% (I - H) %*% hdl
SSE
sig_sq = SSE / (length(age) - 2)
sig_sq
# The approxiamte sigma squared value is: 99.06937

# 3) d)
model = lm(hdl~age)
model
# The equation for the linear regression model is:
#       hdl = 50.7841 + (-0.1298 * age)
summary(model)
# The approximate value of MSE is:
#     9.953^2 = 99.06221

# 3) e)
# The calculated values from the matrix method and the lm function are almost identical.

####################################################################################################