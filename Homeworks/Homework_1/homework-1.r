# 1) a)
# b1 = Sxy / Sxx
#   Sxy = S((xi - x_bar) * yi) = (125471.10 - (1 / 250) * 6322.28 * 4757.90)
#   Sxx = S(xi - x_bar) ^ 2 = (162674.18 - (1 / 250) * 6322.28 ^ 2)
b1 = (125471.10 - (1 / 250) * 6322.28 * 4757.90) / ((162674.18 - (1 / 250) * 6322.28 ^ 2))
b1
# The slope is approximatly: 1.845635

# b0 = y_bar - b1 * x_bar = (1 / n) * Syi - (b1 * (1 / n) * Sxi = (1 / 250) * 4757.9 - (b1 * (1 / 250) * 6322.28)
b0 = (1 / 250) * 4757.9 - (b1 * (1 / 250) * 6322.28)
b0
# The intercept is approximatly: -27.64287
# The equation for the regression line is:
#       y = b1 * x + b0
#       y = (1.845635 * x) - 27.64287
#               OR
#       Body Fat = (1.845635 * BMI) - 27.64287

# 1) b)
bodyfat = (b1 * 30) + b0
bodyfat
# The bodyfat will be approximatly 27.72616% for a man with a BMI of 30
####################################################################################################


# 2)
diameter = c(32, 29, 24, 45, 20, 30, 26, 40, 24, 18)
volume = c(185, 109, 95, 300, 30, 125, 55, 246, 60, 15)
cor.test(diameter, volume)
# Based on the p-value of 8.217e-07, we reject the null hypothesis that the correlation between
#   trunk diameter and wood volume is 0 and conclude that there is a correlation. Therefore, we
#   can say that there does indeed appear to be a linear relationship between diameter and volume.


####################################################################################################


# 3) a)
library(foreign)
football_data = read.csv("Homework1-3.csv")
head(football_data)
attach(football_data)
model = lm(Rating.points~Yards.per.Attempt)
model
# The equation for the linear regression line is:
#       Rating.points = (10.09*Yards.per.Attempt) + 14.20
png("Homework_1_QN_3_a.png")
plot(Yards.per.Attempt, Rating.points)
abline(model)
dev.off()

# 3) b)
summary(model)
# The least square estimate for the intercept is: 14.195
# The least square estimate for the slope is: 10.092

# 3) c)
predict(model, data.frame(Yards.per.Attempt=7.5))

# 3) d)
predict(model, data.frame(Yards.per.Attempt=6.5))
rating_change = 89.88357-79.79183
rating_change
# The approximate change in rating for a decrease in one yard per attempt is: 10.09174

# 3) e)
yard_change = 10 / rating_change
yard_change
# It appears that approximatly a 0.9909094 yard per attempt increase will increase the mean rating by 10

####################################################################################################


# 4) a)
time = c(1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20)
bod = c(0.6, 0.7, 1.5, 1.9, 2.1, 2.6, 2.9, 3.7, 3.5, 3.7, 3.8)
model = lm(bod~time)
model
# The equation for the linear regression line is:
#       bod = (0.1781*time) + 0.6578

# 4) b)
summary(model)
# The standard error is 0.2873
(0.2873)^2
# The estimate of sigma^2 is 0.08254129

# 4) c)
predict(model, data.frame(time=15), level=0.9, interval="conf")
# The 90% confidence interval is: (3.125933, 3.531346)
predict(model, data.frame(time=15), level=0.9, interval="pred")
# The 90% prediction interval is: (2.764355, 3.892924)

# 4) d)
predict(model, data.frame(time=12))
predict(model, data.frame(time=15))
change = 3.328639 - 2.794471
change
# It appears that three days changes the BOD by approximatly 0.534168


####################################################################################################


# 5) a)
windmill_data = read.csv("Homework1-5.csv")
head(windmill_data)
attach(windmill_data)
png("Homework_1_QN_5_a.png")
plot(wind.velocity, DC.output)
dev.off()

# 5) b)
model = lm(DC.output~wind.velocity)
model
# The equation for the linear regression line is:
#       DC.output = (0.2411*wind.velocity) + 0.1309

# 5) c)
confint(model, level=0.9)
# The confidence interval for the intercept is: (-0.08505447, 0.3468047)
# And the confidence interval for the slope is: (0.20850099, 0.2737967)


####################################################################################################