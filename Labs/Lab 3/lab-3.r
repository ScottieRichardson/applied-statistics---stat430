####################################################################################################

# a)
library(MASS)
data(whiteside)
head(whiteside)

####################################################################################################

# b)
attach(whiteside)
png("lab_3_b.png")
plot(Temp, Gas, col=ifelse(Insul=="Before", "red", "blue"), pch=ifelse(Insul=="Before", "B", "A"),
     main="Gas Consumbtion vs. External Temperature")
dev.off()
before = subset(whiteside, Insul == "Before")
head(before)
after = subset(whiteside, Insul == "After")
head(after)

####################################################################################################

# c)
model = lm(Gas~Temp + Insul)
model

png("lab_3_c.png")
plot(Temp, Gas, col=ifelse(Insul=="Before", "red", "blue"), pch=ifelse(Insul=="Before", "B", "A"),
     main="Gas Consumbtion vs. External Temperature")
abline((6.5513 - 1.5652), -0.3367)
abline(model)
legend(8.5, 7.25, pch=c("B", "A"), col=c("red", "blue"), c("Before", "After"))
dev.off()
####################################################################################################
