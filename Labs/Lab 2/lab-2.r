####################################################################################################

# a)
data = read.table("http://media.pearsoncmg.com/aw/aw_sharpe_business_3/datasets/txt/GDP_2013.txt",
                    sep="\t", header=T)
names(data) = c("Year", "GDP")
head(data, 5)
attach(data)
png("lab_2_a.png")
plot(Year, GDP)
dev.off()

####################################################################################################

# b)
model = lm(GDP~Year)
model
# The equation for the linear regression model is:
#           GDP = (0.1993 * Year) - 387.8433

####################################################################################################

# c)
png("lab_2_c.png")
plot(Year, GDP)
abline(model)
dev.off()

####################################################################################################

# d)
summary(model)
# The coefficent of determination is: 0.9673

####################################################################################################

# e)
png("lab_2_e.png")
par(mfrow=c(2,2))
plot(model)
dev.off()

####################################################################################################

# f)
library(MASS)
png("lab_2_f_1.png")
b=boxcox(model)
dev.off()
y1 = GDP^0.25
model2 = lm(y1~Year)
model2
# The new equation for the linear regression model is:
#           y2 = (Year * 0.01224) - 22.66422
summary(model2)
# The new coefficent of determination is: 0.9957
png("lab_2_f_2.png")
par(mfrow=c(2,2))
plot(model2)
dev.off()

####################################################################################################

# g)
# Through the boxcox transformation, the residuals and the Q-Q plot from the second model appear
#   to provide a much better model. And when comparing the coefficent of determination from model 1
#   and model 2, which are 0.9673 and 0.9957 respectivly, we also see an inprovment in the
#   representation of the model for GDP.

####################################################################################################