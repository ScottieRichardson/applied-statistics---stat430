####################################################################################################

# a)
Beers = c(5, 2, 9, 8, 3, 7, 3, 5, 3, 5)
BAL = c(0.10, 0.03, 0.19, 0.12, 0.04, 0.095, 0.07, 0.06, 0.02, 0.05)
model = lm(BAL~Beers)
png("lab1_a.png")
plot(Beers, BAL, main="BAL vs. Number of Beers", xlab="Number of Beers", ylab="Blood Alchol Level")
abline(model)
dev.off()

####################################################################################################

# b)
confint(model, level=.95)
# The 95% confidence interval for the intercept is: (-0.06284414, 0.02584414)
# The 95% confidence interval for the slope is: (0.01110391, 0.02729609)

####################################################################################################

# c)
model
# The equation for the linear regression model is:
#           BAL = (0.0192 * Beers) - 0.0185

####################################################################################################

# d)
predict(model, data.frame(Beers=6), level=0.95, interval="conf")
# The 95% confidence interval is: (0.0768687, 0.1165313)
predict(model, data.frame(Beers=6), level=0.95, interval="pred")
# The 95% prediction interval is: (0.03611438, 0.1572856)

####################################################################################################

# e)
anova(model)
# The MSE is approximatly 0.0006163

####################################################################################################

# f)
summary(model)
# The value of the coefficient of determination is 0.789

####################################################################################################