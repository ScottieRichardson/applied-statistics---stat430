####################################################################################################

# 1) a)
cars_sold = c(8149, 7316, 4778, 4580, 4506, 4454, 2535, 2291, 2278, 1187)
revenues = c(1996, 2118, 1174, 943, 1813, 1175, 628, 605, 465, 447)
model = lm(revenues~cars_sold)
anova(model)
# Yes, using a significance level of 0.05, the number of cars sold is an important predictor
#   variable for the model.

# 1) b)
confint(model)
# The 95% confidence interval for the regression coefficient is: (0.1718915, 0.3531304)

# 1) c)
confint(model, level=0.9)
# The 90% confidence interval for the regression coefficient is: (0.189436, 0.335586)

# 1) d)
summary(model)
# The coefficient of determination is: 0.848

# 1) e)
summary(model)
sigma = 264
sig_sq = sigma^2
sig_sq
# The value of sigma squared is: 69696

####################################################################################################

# 2) a)
weight = c(62.0, 75.0, 73.0, 82.0, 76.2, 95.7, 59.4, 93.4, 82.1, 68.9, 71.7, 82.1, 83.0, 71.0, 64.0,
           67.0)
glucose = c(102, 103, 104, 112, 102, 121, 79, 117, 101, 75, 99, 115, 118, 104, 102, 97)
model = lm(glucose~weight)
model
# The equation of the linear regression model is:
#   glucose = 34.1501 + (0.9155 * weight)

# 2) b)
anova(model)
# Using a significance level of 0.05, body weight is indeed an important predictor variable for
#   the model.

# 2) c)
library(MASS)
b = boxcox(model, lambda=seq(-10,10,.1))

# Our approximate lmabda value is 5
lambda = 5
y2 = (glucose^lambda - 1) / lambda
model2 = lm(y2~weight)
model2
# The equation of the new linear regression model is:
#   y2 = 20.0360 + (0.2883 * weight)

# 2) d)
png("2_d_model_1.png")
par(mfrow=c(2,2))
plot(model)
dev.off()

png("2_d_model_2.png")
par(mfrow=c(2,2))
plot(model2)
dev.off()

summary(model)
summary(model2)

# Though the over-all shape of the residuals vs fitted graph is almost identical between the
#   first and second models, the scale of the y has been reduced. However, when we look at the
#   coefficient of determination of the two models, 0.5769 for model #1 and 0.5644 for model #2,
#   we can see that we seem to have made the model worse through the box-cox transformation.

####################################################################################################

# 3) a)
library(faraway)
data(sexab)
head(sexab)

# 3) b)
attach(sexab)
png("3_b.png")
plot(cpa, ptsd, col=ifelse(csa=="Abused", "red", "blue"), pch=ifelse(csa=="Abused", "A", "N"),
     main="Childhood Abuse vs PTSD", xlab="Childhood Physical Abuse (PSA)",
     ylab="Post-Traumatic Stress Disorder (PTSD)")
legend(-3.5, 19.75, pch=c("A", "N"),
     col=c("red", "blue"), c("Abused Sexually", "Not Abused Sexually"))
dev.off()

# 3) c)
model = lm(ptsd~cpa+csa)
model
# The equation for the linear regression model for those who were abused is:
#   ptsd = 10.2480 + (0.5506 * cpa)

# The equation for the linear regression model for those who were NOT abused is:
#   ptsd = 3.9752 + (0.5506 * cpa)

png("3_c.png")
plot(cpa, ptsd, col=ifelse(csa=="Abused", "red", "blue"), pch=ifelse(csa=="Abused", "A", "N"),
     main="Childhood Abuse vs PTSD", xlab="Childhood Physical Abuse (PSA)",
     ylab="Post-Traumatic Stress Disorder (PTSD)")
legend(-3.5, 19.75, pch=c("A", "N"),
     col=c("red", "blue"), c("Abused Sexually", "Not Abused Sexually"))
     abline(model, col="red", lwd=2)
     abline(3.9752, 0.5506, col="blue", lwd=2)
dev.off()

####################################################################################################

# 4) a)
library(MASS)
data(hills)
head(hills)

# 4) b)
attach(hills)
model = lm(time~dist+climb)
model
# The equation for the linear regression model is:
#   time = -8.99204 + (6.21796 * dist) + (0.01105 * climb)

# 4) c)
png("4_c.png")
par(mfrow=c(2,2))
plot(model)
dev.off()
anova(model)
summary(model)
# Based on the residuals vs fitted graph, p-values of the anova test and the coefficient of
#   determination (Multiple R-squared) from the model summary, we can see that our model appear
#   to be a rather good model of the data.

# 4) d)
time_estimate = -8.99204 + (6.21796 * 3) + (0.01105 * 2500)
time_estimate
# The estimate time taken to complete a 3-mile distance and climbing 2,500 feet is:
#   37.28684 minutes

predict(model, data.frame(dist=c(3), climb=c(2500)), level=0.9, interval="conf")
# The 90% confidence interval for the estimated time to complete a 3-mile distance and climbing
#   2,500 feet is: (29.60772, 44.95549)

predict(model, data.frame(dist=c(3), climb=c(2500)), level=0.9, interval="pred")
# The 90% prediction interval for the estimated time to complete a 3-mile distance and climbing
#   2,500 feet is: (11.26539, 63.29782)

####################################################################################################
# 5)
# What is the difference between Type I sum of squares and Type III sum of squares?

# The difference is that Type I sum of squares, also called the sequential sum of squares, only
#   measures improvement by adding regressor variables in-order sequentially. However, Type III sum
#   of squares, also called the partial sum of squares, measures the effect every regressor variable
#   individually has on improvement of the model.

####################################################################################################

